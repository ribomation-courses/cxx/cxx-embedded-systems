#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <numeric>
#include <cmath>

using namespace std;
using namespace std::literals;

template<typename T>
auto operator <<(ostream& os, const vector<T>& v) -> ostream& {
    os << "[";
    if (!v.empty()) {
        os << v[0];
        for (auto k = 1UL; k < v.size(); ++k) os << ", " << v[k];
    }
    os << "]";
    return os;
}

int main() {
    auto in      = istream_iterator<double>{cin};
    auto eof     = istream_iterator<double>{};
    auto numbers = vector<double>{in, eof};
    //cout << "numbers: " << numbers << endl;

//    auto smallest = min_element(begin(numbers), end(numbers));
//    auto largest = max_element(begin(numbers), end(numbers));
    auto[smallest, largest] = minmax_element(begin(numbers), end(numbers));
    cout << "min/max: " << *smallest << " / " << *largest << endl;

    auto mean = accumulate(begin(numbers), end(numbers), 0.0) / numbers.size();
    cout << "mean: " << mean << endl;

    auto stddev = sqrt(accumulate(begin(numbers), end(numbers), 0.0, [mean](auto sum, auto x) {
        return sum + (x - mean) * (x - mean);
    }) / numbers.size());
    cout << "stddev: " << stddev << endl;

    return 0;
}
