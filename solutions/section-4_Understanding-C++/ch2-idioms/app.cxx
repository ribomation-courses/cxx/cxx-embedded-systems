#include <iostream>
#include "sparse-vector.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation::types;

int main() {
    auto v = SparseVector<int>{};
    cout << "v: " << v << " (" << v.capacity() << ")" << endl;

    v[5] = 42;
    cout << "v: " << v << " (" << v.capacity() << ")" << endl;

    v[25] = 4242;
    cout << "v: " << v << " (" << v.capacity() << ")" << endl;

    cout << "v[5]=" << v[5]
         << ", v[6]=" << v[6]
         << ", v[25]=" << v[25]
         << ", v[24]=" << v[24]
         << " (" << v.capacity() << ")" << endl;

    return 0;
}

