cmake_minimum_required(VERSION 3.14)
project(01_idioms)

set(CMAKE_CXX_STANDARD 17)
add_compile_options(-Wall -Wextra -Werror -Wfatal-errors)

add_executable(sparse-vector
        sparse-vector.hxx
        app.cxx
        )

