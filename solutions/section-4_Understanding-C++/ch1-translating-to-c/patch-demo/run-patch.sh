#!/usr/bin/env bash
set -uex

CC="gcc"
CCXX="g++ -std=c++17"
WARN="-Wall -Wextra -Wfatal-errors"
LDFLAGS="-Wl,--allow-multiple-definition"

SRC="./patch-demo/src"
BIN="./patch-demo/bld"

rm -rf ${BIN}
mkdir -p ${BIN}

${CCXX} ${WARN} -c ${SRC}/func.cxx -o ${BIN}/func.o
${CCXX} ${WARN} -c ${SRC}/func-app.cxx -o ${BIN}/func-app.o

rm -f ${BIN}/func-lib.a
ar crs ${BIN}/func-lib.a ${BIN}/func.o ${BIN}/func-app.o
${CCXX} ${BIN}/func-lib.a -o ${BIN}/func-app

${CC} ${WARN} -c ${SRC}/patched-func.c -o ${BIN}/patched-func.o
${CCXX} ${LDFLAGS} ${BIN}/patched-func.o ${BIN}/func-lib.a -o ${BIN}/patched-func-app

set +x
echo "--- Running original function ---"
${BIN}/func-app

echo "--- Running patched function ---"
${BIN}/patched-func-app

