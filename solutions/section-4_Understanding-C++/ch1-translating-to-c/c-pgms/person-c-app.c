#include <stdio.h>
#include <stdlib.h>

struct Person;
extern struct Person*   _Z9newPersonPKcj(const char*, unsigned);    //factory
extern void*            _ZN6PersonD1Ev(struct Person*);             //destructor
const char*             _ZNK6Person7getNameEv(struct Person*);      //getName
extern unsigned         _ZNK6Person6getAgeEv(struct Person*);       //getAge
extern void             _ZN6Person7incrAgeEv(struct Person*);       //incrAge
// $ find . -name '*person*.o'
// ./cmake-build-debug/CMakeFiles/person-c.dir/person/person.cxx.o
// $ nm ./cmake-build-debug/CMakeFiles/person-c.dir/person/person.cxx.o | grep -i person
//000000000000018a T _Z9newPersonPKcj
//000000000000016e T _ZN6Person7incrAgeEv
//0000000000000000 T _ZN6PersonC1EPKcj
//0000000000000000 T _ZN6PersonC2EPKcj
//00000000000000f8 T _ZN6PersonD1Ev
//00000000000000f8 T _ZN6PersonD2Ev
//000000000000015c T _ZNK6Person6getAgeEv
//0000000000000142 T _ZNK6Person7getNameEv

int main(int argc, char** argv) {
    printf("Welcome to C\n");

    //auto p = new Person(...)
    struct Person* p = _Z9newPersonPKcj("Justin Time", 42);

    //cout << p->getName() << p->getAge()
    printf("p: %s, %d @ %p\n", _ZNK6Person7getNameEv(p), _ZNK6Person6getAgeEv(p), p);

    // p->incrAge()
    _ZN6Person7incrAgeEv(p);

    //cout << p->getName() << p->getAge()
    printf("p: %s, %d @ %p\n", _ZNK6Person7getNameEv(p), _ZNK6Person6getAgeEv(p), p);

    // delete p
    free(_ZN6PersonD1Ev(p));

    return 0;
}
