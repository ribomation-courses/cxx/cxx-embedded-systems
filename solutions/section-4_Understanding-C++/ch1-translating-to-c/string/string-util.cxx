#include <algorithm>
#include <cstring>
#include <cctype>
#include "string-util.hxx"

const char* toUpperCase(const char* s) {
    const auto size = strlen(s);
    char* buf = new char[size + 1];
    std::transform(s, s + size, buf, [](auto ch) {
        return ::toupper(ch);
    });
    buf[size] = '\0';
    return buf;
}

