#include <iostream>
#include "string-util.hxx"

int main(int argc, char** argv) {
    const char* src = "tjabba habba babba";
    auto dst = toUpperCase(src);
    std::cout << "[C++] " << src << " --> " << dst << "\n";
    delete dst;
    return 0;
}
