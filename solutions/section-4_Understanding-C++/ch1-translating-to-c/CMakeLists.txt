cmake_minimum_required(VERSION 3.14)
project(ch2_translating_to_c__december_)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_C_STANDARD 99)

add_executable(string-app
        string/string-util.hxx
        string/string-util.cxx
        string/string-util-app.cxx
        )

add_executable(string-c
        c-pgms/string-c-app.c
        string/string-util.cxx
        )


add_executable(person-cxx
        person/person.hxx
        person/person.cxx
        person/person-app.cxx
        )

add_executable(person-c
        c-pgms/person-c-app.c
        person/person.cxx
)

