#include <iostream>
#include "person.hxx"

using namespace std;

int main() {
    cout << "Welcome to C++\n";
    {
        auto p = Person{"Anna Conda", 42};
        cout << "p: " << p.getName() << ", " << p.getAge() << endl;
    }
    {
        auto p = new Person{"Per Silja", 37};
        cout << "p: " << p->getName() << ", " << p->getAge() << endl;
        delete p;
    }
    return 0;
}
