#include <iostream>
#include "person.hxx"

using namespace std;

Person::Person(const char* name_, unsigned age_)
        : name{name_}, age{age_} {
    cout << "Person{" << name << ", " << age << "} @ " << this << endl;
}

Person::~Person() {
    cout << "~Person{} @ " << this << endl;
}

const char* Person::getName() const {
    return name.c_str();
}

unsigned Person::getAge() const { return age; }

void Person::incrAge() { ++age; }

Person* newPerson(const char* name_, unsigned age_) {
    return new Person(name_, age_);
}

