#pragma once

#include <string>

class Person {
    const std::string name;
    unsigned          age;
public:
    Person(const char* name, unsigned age);
    ~Person();
    const char* getName() const;
    unsigned getAge() const;
    void incrAge();
};

extern Person* newPerson(const char* name, unsigned age);


