#include <iostream>
using namespace std;

class Account {
    int balance = 42;
public:
    int get() const {return balance;}
    void update(int amount) {balance += amount;}
};

void run(const Account& acc, const int amount) {
    cout << "Account{EUR " << acc.get() << "}" << endl;

    //acc.update(amount); //error: passing "const Account" as "this" argument discards qualifiers
    const_cast<Account&>(acc).update(amount);

    cout << "Account{EUR " << acc.get() << "}" << endl;
}

int main() {
    auto account = Account{};
    run(account, -15);
    return 0;
}
