#include <iostream>
#include "person.hxx"
using namespace std;
using namespace ribomation;

void func1(Person q) {
    cout << "[func1] q: " << q.toString() << endl;
    q.incrAge();
    cout << "[func1] q: " << q.toString() << endl;
}

auto func2() -> Person {
    return {"Anna Conda", 42};
}

int main() {
    auto p = Person{"Cris P. Bacon", 27};
    cout << "[main] (1) p: " << p.toString() << endl;

    cout << "------\n";
    func1(p);
    cout << "[main] (2) p: " << p.toString() << endl;

    cout << "------\n";
    func1(move(p));
    cout << "[main] (3) p: " << p.toString() << endl;

    cout << "------\n";
    p = func2();
    cout << "[main] (4) p: " << p.toString() << endl;

    return 0;
}
