#pragma once
#include <iostream>
#include <sstream>
#include <cstring>

namespace ribomation {
    using namespace std;

    class Person {
        char* name = nullptr;
        unsigned   age = 0;
        static int instanceCount;
    public:
        ~Person();
        Person();
        Person(const char* n, unsigned a);
        Person(const Person& that);
        Person(Person&& that) noexcept;

        auto operator =(const Person& that) -> Person&;
        auto operator =(Person&& that) noexcept -> Person&;

        string toString() const;
        unsigned incrAge();
    private:
        void dump(const char* , ostream& );
    };
}
