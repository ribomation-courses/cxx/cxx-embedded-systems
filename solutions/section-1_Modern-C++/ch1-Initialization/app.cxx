#include <iostream>
#include <string>
#include <vector>
using namespace std;
using namespace std::literals;

int main() {
    {
        vector<string> words = {"hi"s, "hello"s, "howdy"s, "goodday"s, };
        for (string& w : words) cout << w << " ";
        cout << "\n";
    }

    {
        string sentence = "It was a dark and silent night"s;
        if (int pos = sentence.find("and"s); pos != string::npos) {
            cout << "found: " << sentence.substr(pos) << endl;
        }

        if (int pos = sentence.find("night"s); pos != string::npos) {
            cout << "found: " << sentence.substr(pos) << endl;
        }

        if (int pos = sentence.find("foobar"s); pos != string::npos) {
            cout << "found: " << sentence.substr(pos) << endl;
        } else {
            cout << "!found: pos=" << pos << endl;
        }
    }

    return 0;
}

