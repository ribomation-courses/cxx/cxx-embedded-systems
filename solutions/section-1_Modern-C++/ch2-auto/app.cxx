#include <iostream>
#include <iomanip>
#include <map>
#include <tuple>

using namespace std;
using namespace std::literals;

auto fibonacci(unsigned n) {
    if (n == 0) return 0U;
    if (n == 1) return 1U;
    return fibonacci(n - 2) + fibonacci(n - 1);
}

auto mk(unsigned n) {
    return make_tuple(n, fibonacci(n));
}

struct Data {unsigned arg; unsigned result;};
auto mk2(unsigned n) {
    return Data{n, fibonacci(n)};
}

auto compute(unsigned n) {
    auto      values = map<unsigned, unsigned>{};
    for (auto k      = 1U; k <= n; ++k) {
        auto [arg, result] = mk2(k);
        values[arg] = result;
    }
    return values;
}

int main(int argc, char** argv) {
    auto N = (argc <= 1) ? 10U : stoi(argv[1]);
//    auto result = fibonacci(N);
//    auto [arg, result] = mk(N);
//    cout << "fib("<<arg<<") = " << result << endl;
    for (auto [arg, fib] : compute(N)) {
        cout << "fib(" << setw(2) << arg << ") = " << setw(10) << fib << endl;
    }
    return 0;
}
