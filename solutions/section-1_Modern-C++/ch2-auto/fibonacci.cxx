#include <iostream>
#include <string>
#include <map>
#include <tuple>

using namespace std;
using namespace std::literals;

auto fibonacci(unsigned n) {
    if (n == 0) return 0U;
    if (n == 1) return 1U;
    return fibonacci(n - 2) + fibonacci(n - 1);
}

struct Data {
    unsigned arg;
    unsigned result;
    Data(unsigned arg, unsigned result)
            : arg(arg), result(result) {}
};

auto compute(unsigned n) /*-> Data*/ {
    return Data{n, fibonacci(n)};
}
auto compute2(unsigned n) {
    return make_tuple(n, fibonacci(n));
}

auto populate(unsigned n) /*-> map<unsigned, unsigned>*/ {
    auto result = map<unsigned, unsigned>{};
    for (auto k=1U; k<=n; ++k) {
        auto [arg, res] = compute2(k);
        result[arg] = res;
    }
    return result;
}

int main(int argc, char** argv) {
    auto N = argc == 1 ? 10U : stoi(argv[1]);

    // (1)
    //auto result = fibonacci(N);
    //cout << "fib(" << N << ") = " << result << endl;

    // (2)
//    auto[arg, res] = compute(N);
//    cout << "fib(" << arg << ") = " << res << endl;

    // (3)
    for (auto [arg, res] : populate(N)) {
        cout << "fib(" << arg << ") = " << res << endl;
    }

    return 0;
}

