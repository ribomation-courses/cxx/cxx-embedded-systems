cmake_minimum_required(VERSION 3.14)
project(ch2_auto)

set(CMAKE_CXX_STANDARD 17)

add_executable(app app.cxx)
add_executable(fibonacci fibonacci.cxx)

