#include <iostream>
#include <algorithm>

using namespace std;
using namespace std::literals;

int main() {
    int        numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
    auto const N         = sizeof(numbers) / sizeof(numbers[0]);

    for_each(numbers, numbers + N, [](auto x) {
        cout << x << " ";
    });
    cout << "\n";

    auto factor = 42;
    transform(numbers, numbers + N, numbers, [factor](auto x) mutable {
        cout << "[lambda] factor=" << factor << endl;
        return x * factor;
    });
    cout << "[main] factor=" << factor << endl;

    for_each(numbers, numbers + N, [](auto x) {
        cout << x << " ";
    });
    cout << "\n";

    return 0;
}
