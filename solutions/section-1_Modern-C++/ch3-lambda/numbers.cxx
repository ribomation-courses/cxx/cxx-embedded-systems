#include <iostream>
#include <algorithm>

using namespace std;

int main() {
    int        numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    auto const N         = sizeof(numbers) / sizeof(numbers[0]);
    for_each(numbers, numbers + N, [](auto n) { cout << n << " "; });
    cout << endl;

    auto factor = 42;
    cout << "factor: " << factor << endl;
    transform(numbers, numbers + N, numbers, [&factor](auto n)  {
        //cout << " **factor: " << factor << endl;
        return n * factor++;
    });
    cout << "factor: " << factor << endl;

    for_each(numbers, numbers + N, [](auto n) { cout << n << " "; });
    cout << endl;

    return 0;
}
