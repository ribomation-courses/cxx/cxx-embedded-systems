extern void unit1(int);
extern void unit2(int);

//multiple definition of `magic(int)'
//error: ld returned 1 exit status

//$ nm --demangle ./cmake-build-debug/CMakeFiles/unit.dir/units/unit-app.cxx.o
//        U unit1(int)
//        U unit2(int)
//0000000000000000 T main

//$ nm --demangle ./cmake-build-debug/CMakeFiles/unit.dir/units/unit-1.cxx.o
//0000000000000000 T magic(int)
//000000000000003d T unit1(int)

void tjollahopp(int,float,char) {}

int main() {
    unit1(10);
    unit2(10);
    return 0;
}
