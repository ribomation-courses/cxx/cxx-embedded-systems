#include <iostream>
#include <memory>
#include "person.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

auto func1(unique_ptr<Person> q) -> unique_ptr<Person> {
    q->incrAge();
    cout << "[func1] q: " << *q << endl;
    return q;
}

auto func2(shared_ptr<Person> q) -> shared_ptr<Person> {
    q->incrAge();
    cout << "[func2] q: " << *q << endl;
    return q;
}

void use_unique_ptr() {
    auto anna = make_unique<Person>("Anna Conda"s, 42);
    cout << "[use_unique_ptr] anna: " << *anna << endl;
    auto p = func1(move(anna));
    cout << "[use_unique_ptr] p: " << *p << endl;
    cout << "[use_unique_ptr] anna: " << anna.get() << endl;
}


void use_shared_ptr() {
    auto justin = make_shared<Person>("Justin Time"s, 37);
    cout << "[use_shared_ptr] justin: " << *justin << endl;
    cout << "[use_shared_ptr] justin.refcnt: " << justin.use_count() << endl;
    auto p = func2(justin);
    cout << "[use_shared_ptr] p: " << *p << endl;
    cout << "[use_shared_ptr] justin: " << *justin << endl;
    cout << "[use_shared_ptr] justin.refcnt: " << justin.use_count() << endl;
}

int main() {
    use_unique_ptr();
    cout << "---------------\n";
    use_shared_ptr();
    return 0;
}