#include <iostream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include "account.hxx"


namespace ribomation::types {
    using namespace std;
    using namespace std::literals;

    const auto CAPACITY = 12;
    vector<Account>    Account::storage{CAPACITY};
    vector<bool>       Account::used(CAPACITY);

    void* Account::operator new(size_t) {
        cout << "Account::operator new()";

        if (auto idx = find_if(used.begin(), used.end(), [](auto x) { return !x; }); idx != used.end()) {
            *idx = true;
            auto addr = &storage.at(static_cast<unsigned long>(distance(used.begin(), idx)));
            cout << "  addr=" << addr << endl;
            return addr;
        } else {
            throw overflow_error{"no more slots"};
        }
    }

    void Account::operator delete(void* ptr) {
        cout << "Account::operator delete() ptr=" << ptr << endl;

        if (auto idx = find_if(storage.begin(), storage.end(), [=](auto& x) { return ptr == &x; });
                idx != storage.end())
        {
            used.at(static_cast<unsigned long>(distance(storage.begin(), idx))) = false;
        } else {
            throw overflow_error{"cannot find the mem blk"};
        }
    }

    string Account::toString() const {
        ostringstream buf;
        buf << "Account{SEK " << balance << ", " << rate << "%} @ " << this;
        return buf.str();
    }

}
