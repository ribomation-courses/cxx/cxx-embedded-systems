#include <iostream>
#include <iomanip>
#include <bitset>
#include <cstdint>
#include "pack.hxx"

using namespace std;
using namespace ribomation;

template<unsigned BITS, typename Dst, typename Src>
void useCase(Src upper, Src lower) {
    auto result = pack<Dst>(static_cast<Src>(upper), static_cast<Src>(lower));

    cout << "pack<" << dec << BITS << ">(0x"
         << hex << static_cast<short>(upper) << ", 0x" << static_cast<short>(lower)
         << ") = 0x" << setw(12) << setfill('0') << result
         << " [" << bitset<BITS>{result} << "]\n";
}

int main() {
    auto a = uint8_t{0x12};
    auto b = uint8_t{0x34};

    useCase<16, uint16_t, uint8_t>(a, b);
    useCase<32, uint32_t, uint16_t>(a, b);
    useCase<64, uint64_t, uint32_t>(a, b);

    return 0;
}
