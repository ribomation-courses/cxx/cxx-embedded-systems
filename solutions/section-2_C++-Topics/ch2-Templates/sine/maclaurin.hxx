#pragma once

namespace ribomation::numeric {
    using XXL = unsigned long long;

    constexpr XXL factorial(unsigned n) {
        if (n == 0) return 0;
        if (n == 1) return 1;
        return n * factorial(n - 1);
    }

    constexpr double power(double x, unsigned n) {
        if (n == 0) return 1;
        if (n == 1) return x;
        return x * power(x, n - 1);
    }

    constexpr double maclaurinTerm(double x, unsigned n) {
        auto sign        = power(-1, n);
        auto numerator   = power(x, 2 * n + 1);
        auto denominator = factorial(2 * n + 1);
        return sign * numerator / denominator;
    }

    constexpr double add(double x, unsigned n) {
        if (n == 0) return 1;
        //if (n == 1) return x;
        return maclaurinTerm(x, n) + add(x, n - 1);
    }

    constexpr double sinApprox(double x, unsigned n=10U) {
//        return add(x, 12U);
        double    result = 0;
        for (auto k      = 0U; k <= n; ++k) {
            result += maclaurinTerm(x, k);
        }
        return result;
    }
}
