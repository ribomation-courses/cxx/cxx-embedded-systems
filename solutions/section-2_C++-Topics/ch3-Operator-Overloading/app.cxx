#include <iostream>
#include <sstream>
#include "vector-3d.hxx"
using namespace std;
using namespace std::literals;
using ribomation::Vector3d;

void test_string() {
//    Vector3d<std::string>  v;
//    Vector3d<std::string>  v{"do"s, "not"s, "compile"s};
}

void test_int() {
//    Vector3d<int>  v;
//    Vector3d<int>  v{1,2,3};
}

int main() {
    Vector3d<> v1{2, 5, 10};
    cout << "v1: " << v1 << endl;

    istringstream{"data: <10 20 30>"} >> v1;
    cout << "v1: " << v1 << endl;

    cout << "v1 * 42: " << v1 * 42.F << endl;
    cout << "10 * v1: " << 10.F * v1 << endl;

    Vector3d<> v2{2, 2, 2};
    cout << "v1 + v2: " << v1 + v2 << endl;
    cout << "v1 - v2: " << v1 - v2 << endl;
    cout << "v1 * v2: " << v1 * v2 << endl;

    test_int();
    test_string();
    return 0;
}
