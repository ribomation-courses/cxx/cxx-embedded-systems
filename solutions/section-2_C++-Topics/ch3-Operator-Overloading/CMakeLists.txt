cmake_minimum_required(VERSION 3.14)
project(03_operator_overloading)

set(CMAKE_CXX_STANDARD 17)

add_executable(vectors-3d
        vector-3d.hxx
        app.cxx
        )
#target_compile_options(vectors-3d PRIVATE
#        -Wall -Wextra -Werror -Wfatal-errors
#        )
