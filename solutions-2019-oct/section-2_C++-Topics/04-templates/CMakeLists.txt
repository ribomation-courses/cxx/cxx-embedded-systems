cmake_minimum_required(VERSION 3.14)
project(04_templates)

set(CMAKE_CXX_STANDARD 17)

add_executable(pack
        pack.hxx
        app.cxx)
target_compile_options(pack PRIVATE
        -Wall -Wextra -Werror -Wfatal-errors
        )
