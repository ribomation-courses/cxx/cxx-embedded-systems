#include <iostream>
#include <iomanip>
#include <cstdint>
#include "pack.hxx"

using namespace std;
using namespace ribomation;

int main() {
    auto a = uint8_t{0x12};
    auto b = uint8_t{0x34};

    cout << hex
         << "pack(0x" << static_cast<short>(a)
         << ", 0x" << static_cast<short>(b)
         << ") = 0x" << pack<uint16_t>(a, b) << endl;

    return 0;
}
