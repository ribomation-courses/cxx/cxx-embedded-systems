#include <iostream>
#include <fstream>
#include <string>
#include <stdexcept>
#include <vector>
#include <algorithm>
#include <numeric>
#include <cctype>

using namespace std;
using namespace std::literals;

auto load(istream& is) -> vector<string> {
    auto      words = vector<string>{};
    for (auto word  = ""s; is >> word;) words.push_back(word);
    return words;
}

void print(const vector<string>& words) {
    cout << "words: ";
    for_each(begin(words), end(words), [](auto word) { cout << word << " "; });
    cout << endl;
}

void drain(vector<string>& words) {
    cout << "words: ";
    while (!words.empty()) {
        auto word = words.back();
        words.pop_back();
        cout << word << " ";
    }
    cout << endl;
}

auto totalSize(const vector<string>& words) {
    return accumulate(begin(words), end(words), 0, [](auto sum, auto word) {
        return sum + word.size();
    });
}

auto strip(vector<string>& words) {
    for_each(begin(words), end(words), [](auto& word) {
        auto lastPos = remove_if(begin(word), end(word), [](auto ch) {
            return !::isalpha(ch);
        });
        word.erase(lastPos, end(word));
    });
    return words;
}

int main(int argc, char** argv) {
    auto filename = (argc == 1) ? "./musketeers.txt"s : argv[1];
    auto file     = ifstream{filename};
    if (!file) throw invalid_argument{"cannot open "s + filename};

    // (1)
    auto words = load(file);

    // (2)
    print(words);

    // (3)
    cout << "SUM(size): " << totalSize(words) << endl;

    // (4)
    words.insert(begin(words), "Tjolla-Hopp"s);

    // (5)
    words = strip(words);

    // (6)
    drain(words);

    return 0;
}
