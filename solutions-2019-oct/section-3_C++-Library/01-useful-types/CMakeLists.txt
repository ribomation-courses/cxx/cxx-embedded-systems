cmake_minimum_required(VERSION 3.14)
project(01_useful_types)

set(CMAKE_CXX_STANDARD 17)

add_executable(contact-card
        contact-card.hxx
        app.cxx)

