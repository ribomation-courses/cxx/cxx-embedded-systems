#include <iostream>
#include "contact-card.hxx"
using namespace std;
using namespace std::literals;
using namespace ribomation;

int main() {
    Contact jens = Contact{"Jens Riboe"}
            .setEmail("jens.riboe@ribomation.se"s)
            .setUrl("https://www.ribomation.se/"s)
            .setAddress("Stureplan 4C"s, 11435, "Stockholm"s);

    Contact contacts[] = {
            jens,
            Contact{"Anna Conda"s},
            Contact{"Justin Time"s}.setEmail("justin@foobar.com"s),
            Contact{"Per Silja"s}.setAddress("42 Reboot Lane", 12345, "PWA")
    };

    auto divider = "\n"s + string(20, '-') + "\n"s;
    unsigned id = 1;
    for (auto const& c : contacts)
        cout << "## Contact " << id++ << ":\n" << c << divider;

    return 0;
}
