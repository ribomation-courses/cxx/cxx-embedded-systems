#include <iostream>
#include "sparse-vector.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation::types;

int main() {
    auto v = SparseVector<int>{};
    cout << "v: " << v << " (" << v.capacity() << ")" << endl;

    v[5] = 42;
    cout << "v: " << v << " (" << v.capacity() << ")"<< endl;

    v[25] = 4242;
    cout << "v: " << v << " (" << v.capacity() << ")"<< endl;

    return 0;
}

