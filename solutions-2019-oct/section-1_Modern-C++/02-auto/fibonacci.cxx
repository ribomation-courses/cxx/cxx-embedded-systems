#include <iostream>
#include <map>
#include <tuple>

using namespace std;
using namespace std::literals;

auto fibonacci(unsigned n) {
    if (n == 0) return 0U;
    if (n == 1) return 1U;
    return fibonacci(n - 2) + fibonacci(n - 1);
}

auto mk(unsigned n) {
    return make_tuple(n, fibonacci(n));
}

auto compute(unsigned n) {
    auto      values = map<unsigned, unsigned>{};
    for (auto k      = 1U; k <= n; ++k) {
        auto[arg, result] = mk(k);
        values[arg] = result;
    }
    return values;
}

int main(int argc, char** argv) {
    auto values = compute(42);
    for (auto [arg, fib] : values) {
        cout << "fib(" << arg << ") = " << fib << endl;
    }
    return 0;
}
