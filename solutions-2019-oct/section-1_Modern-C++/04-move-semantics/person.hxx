#pragma once

#include <iostream>
#include <algorithm>
#include <cstring>

namespace ribomation {
    using std::copy;
    using std::ostream;
    using std::cout;
    using std::endl;

    class Person {
        char* name;
        unsigned age;
    public:
        Person(const char* name, unsigned age) : age{age} {
            auto const nameSize = strlen(name);
            this->name = new char[nameSize + 1];
            copy(name, name + nameSize + 1, this->name);
            cout << "Person{" << this->name << "@"
                 << static_cast<const void*>(this->name)
                 << ", " << age << "} @ " << this << endl;
        }

        Person(const Person&) = delete;
        Person& operator =(const Person&) = delete;

        Person(Person&& that) : name{that.name}, age{that.age} {
            that.name = nullptr;
            that.age  = 0;
            cout << "Person{&& " << name << "@" << static_cast<const void*>(name) << ", " << age << "} @ " << this << endl;
        }

        Person& operator =(Person&& that) {
            if (this != &that) {
                delete[] name;
                this->name = that.name;
                this->age  = that.age;
                that.name  = nullptr;
                that.age   = 0;
            }
            cout << "operator=(Person&& " << name << "@" << static_cast<const void*>(name) << ") @ " << this << endl;
            return *this;
        }

        ~Person() {
            cout << "~Person("
                 << (name != nullptr ? name : "") << "@"
                 << static_cast<void*>(name) << ") @ " << this << endl;
            delete[] name;
        }

        friend ostream& operator <<(ostream& os, const Person& p) {
            return os << "Person{" << p.name << ", " << p.age << "} @ " << &p;
        }
    };

}
