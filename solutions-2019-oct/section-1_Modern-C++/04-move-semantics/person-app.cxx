#include <iostream>
#include "person.hxx"

using namespace std;
using namespace ribomation;

Person func(Person p) {
    cout << "[func] enter\n";
    cout << "[func] p: " << p << "\n";
    cout << "[func] exit\n";
    return p;
}

int main() {
    cout << "[main] enter\n";
    {
        auto p = Person{"Anna Conda", 42};
        cout << "[main] p: " << p << "\n";

        p = func(move(p));
        cout << "[main] p: " << p << "\n";
    }
    cout << "[main] exit\n";
    return 0;
}
