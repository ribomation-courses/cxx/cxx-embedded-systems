cmake_minimum_required(VERSION 3.14)
project(04_move_semantics)

enable_language(CXX)
set(CMAKE_CXX_STANDARD      17)
set(CMAKE_CXX_STANDARD_REQUIRED YES)
set(CMAKE_CXX_EXTENSIONS        NO)

add_executable(persons
        person.hxx
        person-app.cxx
        )
target_compile_options(persons PRIVATE
        -Wall -Wextra -Werror -Wfatal-errors
        )


