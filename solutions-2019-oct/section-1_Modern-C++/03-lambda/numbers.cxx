#include <iostream>
#include <algorithm>

using namespace std;

int main() {
    int        numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    auto const N         = sizeof(numbers) / sizeof(numbers[0]);
    for_each(numbers, numbers + N, [](auto n) {
        cout << n << " ";
    });
    cout << endl;

    auto /*const*/ factor = 42;
    transform(numbers, numbers + N, numbers, [factor](auto n)  mutable {
        cout << " ** factor: " << factor << endl;
        return n * factor++;
    });
    for_each(numbers, numbers + N, [](auto n) {
        cout << n << " ";
    });
    cout << endl;
    cout << "factor: " << factor << endl;

    return 0;
}
