#include <iostream>
#include <vector>
#include <string>
#include <memory_resource>

using std::cout;
using std::endl;
using std::begin;
using std::end;
using std::data;
using std::size;
using std::move;
using std::uninitialized_fill;

using std::pmr::monotonic_buffer_resource;
using std::pmr::null_memory_resource;
using std::pmr::unsynchronized_pool_resource;
using std::pmr::pool_options;
using std::pmr::set_default_resource;

using std::pmr::vector;
using std::pmr::string;


int main() {
    auto const N = 1600U;
    char       storage[N];
    auto upstream = monotonic_buffer_resource{data(storage), size(storage), null_memory_resource()};
    set_default_resource(&upstream);

    pool_options opts;
    opts.max_blocks_per_chunk        = 5;
    opts.largest_required_pool_block = 32;
    auto memory = unsynchronized_pool_resource{opts};
    auto words  = vector<string>{};

    for (auto k = 1U; k <= 9; ++k) {
        auto txt = string{"outside SSO buffer-00"};
        txt[20] = '0' + k;
        words.push_back(move(txt));
    }
    for (auto const& w : words) cout << "word: [" << w << "]\n";

    return 0;
}


