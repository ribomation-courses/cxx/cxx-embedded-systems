#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
using namespace std::literals;

using UnaryOp = int (*)(int);

auto magic(const vector<int>& numbers, UnaryOp f) -> vector<int> {
    auto result = vector<int>{};
    for (auto const& n : numbers) result.push_back(f(n));
    return result;
}


int main() {
    auto numbers = vector<int>{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    {
        auto threshold = 4;
        cout << "nums > TH: " << count_if(begin(numbers), end(numbers),
                                         [threshold](auto n) { return n > threshold; }) << endl;

        cout << "nums <= TH: " << count_if(begin(numbers), end(numbers),
                                          [&threshold](auto n) { return n <= threshold; }) << endl;
    }

    {
        auto threshold = 6;
        auto cnt = 0;
        cout << "nums > TH: " << count_if(begin(numbers), end(numbers),
                                         [threshold, &cnt](auto n) {
                                             ++cnt;
                                             return n > threshold;
                                         })
             << ", cnt=" << cnt << endl;
    }

    {
        auto threshold = 7;
        auto cnt = 0;
        cout << "nums > TH: " << count_if(begin(numbers), end(numbers),
                                         [threshold, cnt](auto n) mutable {
                                             ++cnt;
                                             return n > threshold;
                                         })
             << ", cnt=" << cnt << endl;
    }

    {
        for (int n : magic(numbers, [](int x) { return x * x; })) cout << n << " ";
        cout << endl;
    }


    return 0;
}

