#include <iostream>
#include <numeric>
#include <etl_lib/vector.h>
#include <etl_lib/algorithm.h>
#include <etl_lib/numeric.h>
using namespace std;

template<typename T>
ostream& operator<<(ostream& os, const etl::ivector<T>& v) {
    auto delim = 0U;
    os << "[";
    for (auto n : v) os << (delim++ ? ", " : "") << n;
    os << "] <" << v.size() << ", " << v.capacity() << ">";
    return os;
}

int main(int, char**) {
    etl::vector<int, 10> v;
    cout << "v: " << v << endl;

    for (auto k = 0U; k < v.capacity(); ++k) v.push_back(10 * (k + 1));
    cout << "v: " << v << endl;

    etl::transform(v.begin(), v.end(), v.begin(), v.end(), [](auto n) {
        return n * n;
    });
    cout << "v: " << v << endl;

    auto result = std::accumulate(v.begin(), v.end(), 0, [](auto sum, auto n) {
        return sum + n;
    });
    cout << "result: " << result << endl;

    return 0;
}



