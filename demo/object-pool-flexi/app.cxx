#include <iostream>
#include <iomanip>
#include <string>
#include "circular-memory.hxx"

using namespace std;
using namespace ribomation::memory;

int main(int argc, char** argv) {
    int n = (argc == 1) ? 100 : stoi(argv[1]);

    CircularMemory<60 * sizeof(int)> mem;
    for (auto k = 1; k <= n; ++k) {
        int        * ptr  = new (mem.alloc(sizeof(int))) int{k * 10};
        long       * ptr2 = new (mem.alloc(sizeof(long))) long{k * 42};
        long double* ptr3 = new (mem.alloc(sizeof(long double))) long double{k * 10.333};

        cout << setw(4) << *ptr << " @ " << reinterpret_cast<unsigned long>(ptr)
             << setw(10) << *ptr2 << " @ " << reinterpret_cast<unsigned long>(ptr2)
             << setw(10) << fixed << setprecision(3) << *ptr3 << " @ " << reinterpret_cast<unsigned long>(ptr3)
             << endl;
    }

    return 0;
}
