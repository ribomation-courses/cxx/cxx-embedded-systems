#pragma once

#include <alloca.h>
#include <stdexcept>
#include <vector>
#include <algorithm>
#include <memory>


namespace ribomation::memory {
    using namespace std;
    using byte = unsigned char;

    template<typename Type>
    class Pool {
        const unsigned OBJECT_SIZE = sizeof(Type);
        const unsigned CAPACITY;
        const unsigned STORAGE_SIZE;
        byte* const    storage;
        vector<bool>   usage;

        unsigned nextAvailable() const {
            unsigned idx = 0;
            while (idx < usage.size() && usage.at(idx)) { ++idx; }
            return idx;
        }

        size_t indexOf(const Type* ptr) const {
            return (reinterpret_cast<size_t>(ptr) - reinterpret_cast<size_t>(storage)) / OBJECT_SIZE;
        }

    public:
        Pool(byte* storage, unsigned storageSize) :
                CAPACITY(storageSize / OBJECT_SIZE),
                STORAGE_SIZE(storageSize),
                storage(storage),
                usage(CAPACITY, false) {}

        ~Pool() = default;
        Pool(const Pool<Type>&) = delete;
        Pool<Type>& operator =(const Pool<Type>&) = delete;

        size_t capacity() const { return CAPACITY; }
        size_t free()     const { return capacity() - size(); }
        bool   full()     const { return free() == 0; }
        unsigned size()   const {
            return count_if(usage.begin(), usage.end(), [](auto inUse) {
                return inUse;
            });
        }

        Type* alloc() {
            if (full()) { throw overflow_error("pool full"); }
            const auto idx = nextAvailable();
            usage.at(idx)  = true;
            return reinterpret_cast<Type*>(storage + idx * OBJECT_SIZE);
        }

        void dispose(Type* ptr) {
            ptr->~Type();
            if (byte* a = reinterpret_cast<byte*>(ptr);
                (storage <= a) && (a < (storage + STORAGE_SIZE)))
            {
                usage.at(indexOf(ptr)) = false;
            } else {
                throw overflow_error("object not belonging to this pool");
            }
        }
    };


    template<typename Type>
    struct Deleter {
        Pool<Type>* pool;

        Deleter(Pool<Type>* pool) : pool(pool) {}
        void operator ()(Type* ptr) {
            pool->dispose(ptr);
        }

        Deleter(Deleter<Type>&& that) : pool(that.pool) {
            that.pool = nullptr;
        }
        Deleter<Type>& operator =(Deleter<Type>&& that) {
            pool = that.pool;
            that.pool = nullptr;
            return *this;
        }
    };


    template<typename Type>
    using SmartPtr = unique_ptr<Type, Deleter<Type>>;


    template<typename Type, typename... Arguments>
    SmartPtr<Type> mkPtr(Pool<Type>& pool, Arguments... args) {
        auto ptr = new (pool.alloc()) Type{args...};
        return unique_ptr<Type, Deleter<Type>>{ptr, Deleter<Type>{&pool}};
    };

}


        //cerr << "*** " << __PRETTY_FUNCTION__ << "\n";

