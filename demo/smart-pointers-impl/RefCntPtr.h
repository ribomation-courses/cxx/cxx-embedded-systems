#ifndef __REFCNTPTR__
#define __REFCNTPTR__
#include "SmartPtrBase.h"

template<typename TargetType>
class RefCntPtr : public SmartPtrBase<TargetType> {
	int*			count;
		
	int  value()          const {return *count;}
	bool isNonExclusive() const {return value() > 1;}
	bool isExclusive()    const {return value() == 1;}
	bool isUnreachable()  const {return value() <= 0;}
	int  incr() {return ++(*count);}
	int  decr() {return --(*count);}
	
	void assign(const RefCntPtr<TargetType>& obj) {
		RefCntPtr<TargetType>&  that = const_cast< RefCntPtr<TargetType>& >(obj);
		this->count = that.count;
		this->ptr( that.ptr() );
		incr();
	}
	
	void assign(TargetType* p) {
		this->ptr( p );
		this->count = new int(0);
		incr();
	}
	
	void dispose() {	
		decr();
		if (isUnreachable()) {
			SmartPtrBase<TargetType>::dispose();
			delete count;				
		}
	}

	public:
	RefCntPtr(TargetType* p) { assign(p); }	
	RefCntPtr(const RefCntPtr<TargetType>& p) { assign(p); }
	~RefCntPtr() { dispose(); }
	RefCntPtr<TargetType>&  operator =(const RefCntPtr<TargetType>& obj) {
		if (this != &obj) { dispose(); assign(obj); }
		return *this;
	}
};

#endif
