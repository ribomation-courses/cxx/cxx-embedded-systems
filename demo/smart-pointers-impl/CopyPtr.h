#ifndef __COPYPTR__
#define __COPYPTR__
#include <iostream>
#include "SmartPtrBase.h"

template<typename TargetType>
class CopyPtr : public SmartPtrBase<TargetType> {

	void  assign(const CopyPtr<TargetType>& obj) {
		CopyPtr<TargetType>&  that = const_cast< CopyPtr<TargetType>& >(obj);
		this->ptr( new TargetType(*that) );
	}
	
	void  assign(TargetType* p) { 
		this->ptr(p); 
	}

	public:
	CopyPtr(TargetType* p) { assign(p); }	
	CopyPtr(const CopyPtr<TargetType>& p) { assign(p); }
	~CopyPtr() { this->dispose(); }
	CopyPtr<TargetType>&  operator =(const CopyPtr<TargetType>& ptr) {
		if (this != &ptr) { this->dispose(); assign(ptr); }
		return *this;
	}
};

#endif
