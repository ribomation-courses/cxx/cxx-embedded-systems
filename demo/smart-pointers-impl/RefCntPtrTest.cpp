#include <iostream>
#include <string>
#include "RefCntPtr.h"
#include "trace.h"
#include "Person.h"
using namespace std;

int  InstanceCount::count = 0;

RefCntPtr<Person>    compute(RefCntPtr<Person> ptr) {
	Trace t("compute");
	cout << "[compute] ptr = " << *ptr << endl;
	return ptr;
}

int main() {
	Trace t("main");
	cout << "#persons = " << Person::numObjs() << endl;
	
	{
		Trace b("block-1");
		RefCntPtr<int>  p(new int(17));	
		cout << "p = " << *p << endl;
		*p = *p + 10;
		cout << "p = " << *p << endl;
	}
	
	{
		Trace b("block-2");
		RefCntPtr<Person> nisse(new Person("nisse"));
		cout << "nisse = " << *nisse << endl;
		cout << "#persons = " << Person::numObjs() << endl;
		
		RefCntPtr<Person>  q(nisse);
		try {
			cout << "nisse = " << *nisse << endl;
		} catch (NullPointerException) {cout << "Got NullPointer (nisse) !!!" << endl;}
		try {
			cout << "q = " << *q << endl;
		} catch (NullPointerException) {cout << "Got NullPointer (q) !!!" << endl;}
		
		nisse = compute(q);
		cout << "#persons = " << Person::numObjs() << endl;
		try {
			cout << "nisse = " << *nisse << endl;
		} catch (NullPointerException) {cout << "Got NullPointer (nisse) !!!" << endl;}
		try {
			cout << "q = " << *q << endl;
		} catch (NullPointerException) {cout << "Got NullPointer (q) !!!" << endl;}
	}

	cout << "#persons = " << Person::numObjs() << endl;
	return 0;
}

