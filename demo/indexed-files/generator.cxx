#include <iostream>
#include <string>
#include <random>
#include <algorithm>
#include "account.hxx"
#include "db/indexed-file.hxx"

using namespace std;
using namespace std::literals;
using namespace bank;
using namespace ribomation::io;

Account generateAccount() {
    static random_device           r;
    uniform_int_distribution<char> digit{'0', '9'};
    uniform_int_distribution<char> letter{'A', 'Z'};
    normal_distribution<float>     amount{500, 500};

    string accno = "@@@@-###-#####";
    transform(begin(accno), end(accno), begin(accno), [&](auto ch) {
        return (ch == '#') ? digit(r) : (ch == '@') ? letter(r) : ch;
    });

    return {accno, amount(r)};
}


int main(int argc, char** argv) {
    auto numAccounts = 10U;
//    auto numAccounts = 100'000U;
    auto filename    = "accounts.db"s;
    auto verbose     = true;

    for (auto k            = 1; k < argc; ++k) {
        string arg = argv[k];
        if (arg == "-n") numAccounts = static_cast<unsigned>(stoi(argv[++k]));
        else if (arg == "-f") filename = argv[++k];
        else if (arg == "-v") verbose = true;
        else if (arg == "-q") verbose = false;
    }

    IndexedFile<Account> file{filename, OpenMode::write};
    for (auto            k = 0U; k < numAccounts; ++k) {
        Account acc = generateAccount();
        if (verbose) cout << acc << endl;
        file << acc;
    }
    cout << "Written " << filename
         << ", " << file.size() << " bytes"
         << ", " << file.count() << " records"
         << ", record.size=" << sizeof(Account) << " bytes"
         << endl;

    return 0;
}



