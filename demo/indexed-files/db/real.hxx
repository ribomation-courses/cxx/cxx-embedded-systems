#pragma once

#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <stdexcept>

namespace ribomation::io {
    using namespace std;
    using namespace std::literals;

    template<size_t N, unsigned short D>
    class Real {
        char payload[N];

        size_t last() const {
            size_t k = 0;
            for (; k < N && payload[k]; ++k) {}
            return k;
        }

        void pad(size_t pos) {
            for (auto k = pos; k < N; ++k) payload[k] = '\0';
        }

    public:
        Real() { value(0); }

        Real(float v) { value(v); }

        ~Real() = default;
        Real(const Real<N, D>&) = default;
        Real<N, D>& operator =(const Real<N, D>&) = default;

        float value() const {
            string s{payload, payload + last()};
            return stof(s);
        }

        void value(float v) {
            ostringstream buf;
            buf << setw(N) << setfill(' ') << right << fixed << setprecision(D) << v;
            string s = buf.str();
            if (s.size() > N) {
                throw out_of_range("value(float): too many chars=" + s);
            }
            auto pos = s.copy(payload, N);
            if (pos < N) pad(pos);
        }

        operator float() const { return value(); }

        Real<N, D>& operator =(float v) {
            value(v);
            return *this;
        }

        Real<N, D>& operator +=(float amount) {
            value(value() + amount);
            return *this;
        }
    };

    template<size_t N, unsigned short D>
    inline ostream& operator <<(ostream& os, const Real<N, D>& s) {
        return os << s.value();
    }

    template<size_t N, unsigned short D>
    inline istream& operator >>(istream& is, Real<N, D>& s) {
        char buf[N];
        is.read(buf, N);
        s.value(stof(buf));
        return is;
    }

    template<size_t N, unsigned short D>
    inline bool operator ==(const Real<N, D>& left, const Real<N, D>& right) {
        return abs(left.value() - right.value()) < 1E-10;
    }

    template<size_t N, unsigned short D>
    inline bool operator <(const Real<N, D>& left, const Real<N, D>& right) {
        return left.value() < right.value();
    }

}

