#pragma once

#include <iostream>
#include <string>
#include <stdexcept>

namespace ribomation::io {
    using namespace std;
    using namespace std::literals;

    template<size_t N>
    class Text {
        char payload[N];

        size_t last() const {
            size_t k = 0;
            for (; k < N && payload[k]; ++k) {}
            return k;
        }

        void pad(size_t pos) {
            for (auto k = pos; k < N; ++k) payload[k] = '\0';
        }

    public:
        Text() { pad(0); }
        Text(const string& s) { value(s); }
        ~Text() = default;
        Text(const Text<N>&) = default;
        Text<N>& operator =(const Text<N>&) = default;

        string value() const {
            return {payload, payload + last()};
        }

        void value(const string& s) {
            if (s.size() > N) {
                throw out_of_range("text overflow: N="s + to_string(N) + ", s.size="s + to_string(s.size()));
            }

            auto pos = s.copy(payload, N);
            if (pos < N) pad(pos);
        }

        operator string() const {
            return value();
        }

        Text<N>& operator =(const string& txt) {
            value(txt);
            return *this;
        }
    };

    template<size_t N>
    inline ostream& operator <<(ostream& os, const Text<N>& s) {
        return os << s.value();

    }

    template<size_t N>
    inline istream& operator >>(istream& is, Text<N>& s) {
        char buf[N];
        is.read(buf, N);
        s.value( string{buf} );
        return is;
    }

    template<size_t N>
    inline bool operator ==(const Text<N>& left, const Text<N>& right) {
        return left.value() == right.value();
    }

    template<size_t N>
    inline bool operator <(const Text<N>& left, const Text<N>& right) {
        return left.value() < right.value();
    }

}

