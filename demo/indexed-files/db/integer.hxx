#pragma once

#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <stdexcept>

namespace ribomation::io {
    using namespace std;
    using namespace std::literals;

    template<size_t N>
    class Integer {
        char payload[N];

        size_t last() const {
            size_t k = 0;
            for (; k < N && payload[k]; ++k) {}
            return k;
        }

        void pad(size_t pos) {
            for (auto k = pos; k < N; ++k) payload[k] = '\0';
        }

    public:
        Integer() { value(0); }

        Integer(int v) { value(v); }

        ~Integer() = default;
        Integer(const Integer<N>&) = default;
        Integer<N>& operator =(const Integer<N>&) = default;

        int value() const {
            string s{payload, payload + last()};
            return stoi(s);
        }

        void value(int v) {
            ostringstream buf;
            buf << setw(N) << setfill(' ') << right << v;
            string s = buf.str();
            if (s.size() > N) {
                throw out_of_range("value(int): too many chars=" + s);
            }
            auto pos = s.copy(payload, N);
            if (pos < N) pad(pos);
        }

        operator int() const {
            return value();
        }

        Integer<N>& operator =(int v) {
            value(v);
            return *this;
        }

        Integer<N>& operator +=(int amount) {
            value(value() + amount);
            return *this;
        }

        Integer<N>& operator ++() {
            value(value() + 1);
            return *this;
        }

    };

    template<size_t N>
    inline ostream& operator <<(ostream& os, const Integer<N>& s) {
        return os << s.value();
    }

    template<size_t N>
    inline istream& operator >>(istream& is, Integer<N>& s) {
        char buf[N];
        is.read(buf, N);
        s.value(stoi(buf));
        return is;
    }

    template<size_t N>
    inline bool operator ==(const Integer<N>& left, const Integer<N>& right) {
        return left.value() == right.value();
    }

    template<size_t N>
    inline bool operator <(const Integer<N>& left, const Integer<N>& right) {
        return left.value() < right.value();
    }
}

