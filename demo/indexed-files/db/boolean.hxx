#pragma once

#include <iostream>
#include <string>
#include <stdexcept>

namespace ribomation::io {
    using namespace std;
    using namespace std::literals;

    class Boolean {
        char                  payload;

    public:
        constexpr static char FALSE = 'F';
        constexpr static char TRUE  = 'T';

        Boolean() { value(false); }
        Boolean(bool v) { value(v); }
        ~Boolean() = default;
        Boolean(const Boolean&) = default;
        Boolean& operator =(const Boolean&) = default;

        bool value() const {
            return payload == TRUE;
        }

        void value(bool v) {
            payload = (v ? TRUE : FALSE);
        }

        operator bool() const { return value(); }

        Boolean& operator =(bool v) {
            value(v);
            return *this;
        }
    };

    inline ostream& operator <<(ostream& os, const Boolean& s) {
        return os << (s.value() ? "T" : "F");
    }

    inline istream& operator >>(istream& is, Boolean& s) {
        char buf;
        is.read(&buf, 1);
        s.value(buf == Boolean::TRUE);
        return is;
    }

    inline bool operator ==(const Boolean& left, const Boolean& right) {
        return left.value() == right.value();
    }

    inline bool operator <(const Boolean& left, const Boolean& right) {
        return left.value() < right.value();
    }

}

