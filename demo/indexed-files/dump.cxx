#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <numeric>
#include "account.hxx"
#include "db/indexed-file.hxx"

using namespace std;
using namespace std::literals;
using namespace bank;
using namespace ribomation::io;

void prettyDump(const string& filename) {
    IndexedFile<Account> file{filename, OpenMode::read};
    for (auto acc : file) cout << acc << endl;
}

void rawDump(const string& filename) {
    fstream file{filename, ios_base::in | ios_base::binary};
    if (!file) throw invalid_argument{"cannot open "s + filename};

    const auto N = sizeof(Account);
    for (char buf[N]; file.read(buf, N);) {
        string payload{buf, buf + N};
        transform(payload.begin(), payload.end(), payload.begin(), [](char ch) -> char {
            return (ch == '\0') ? ' ' : ch;
        });
        cout << "[" << payload << "]\n";
    }
}

double totalBalance(const string& filename) {
    IndexedFile<Account> file{filename, OpenMode::read};
    return accumulate(file.begin(), file.end(), 0, [](auto sum, auto acc) {
        return sum + acc.getBalance();
    });
}

int main(int argc, char** argv) {
    auto filename = "accounts.db"s;
    auto raw      = false;

    for (auto k = 1; k < argc; ++k) {
        string arg = argv[k];
        if (arg == "-f") filename = argv[++k];
        else if (arg == "-r") raw = true;
        else if (arg == "-p") raw = false;
    }

    if (raw)
        rawDump(filename);
    else
        prettyDump(filename);
    cout.imbue(locale{"en_US.UTF8"});
    cout << "Balance: SEK " << fixed << setprecision(2) << totalBalance(filename) << endl;

    return 0;
}
