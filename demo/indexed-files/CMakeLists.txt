cmake_minimum_required(VERSION 3.10)
project(indexed_files)

set(CMAKE_CXX_STANDARD 17)
add_compile_options(-Wall -Wextra  -Wfatal-errors -Wno-unused-variable)

set(DB_FILES
        db/text.hxx
        db/integer.hxx
        db/real.hxx
        db/boolean.hxx
        db/indexed-file.hxx
        )

add_executable(generator
        ${DB_FILES}
        account.hxx
        generator.cxx
        )

add_executable(dump
        ${DB_FILES}
        account.hxx
        dump.cxx
        )

add_executable(update
        ${DB_FILES}
        account.hxx
        update.cxx
        )
