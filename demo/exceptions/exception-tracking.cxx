#include <iostream>
#include <string>
#include <exception>
#include "trace.hxx"
using namespace std;
using namespace std::literals;
using namespace ribomation;

void func3(int idx) {
    auto f   = Trace{"func3()"};
    auto txt = "abc"s;
    if (idx >= txt.size()) f.print("*boom*");
    auto x = txt.at(idx);
}
void func2(int idx) {
    auto f = Trace{"func2()"};
    func3(idx);
}
void func1(int idx) {
    auto f = Trace{"func1()"};
    func2(idx);
}

int main() {
    auto m = Trace{"main"};
    try {
        auto t = Trace{"try-1"};
        func1(0);
    } catch (const exception& x) { cout << "ERROR: " << x.what() << endl; }
    try {
        auto t = Trace{"try-2"};
        func1(3);
    } catch (const exception& x) { cout << "ERROR: " << x.what() << endl; }
    return 0;
}

