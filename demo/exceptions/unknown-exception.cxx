#include <string>
#include <exception>
#include "trace.hxx"
using namespace std;
using namespace std::literals;
using namespace ribomation;

void bizop() {
    auto t = Trace{"bizop"};
    t.print("*boom*");
    auto txt = "abc"s;
    txt.at(5);
}

void handleError(exception_ptr xptr) {
    auto t = Trace{"handleError"};
    try {
        if (xptr) rethrow_exception(xptr);
    } catch (const exception& x) {
        t.print(x.what());
    }
}

int main() {
    auto m = Trace{"main"};
    try {
        auto t = Trace{"try"};
        bizop();
    } catch (...) {
        auto c = Trace{"catch"};
        handleError(current_exception());
    }
    return 0;
}

