#include <iostream>
#include <string>
using namespace std;
using namespace std::literals;

int main() {
    string fromCharPtrLiteral = "abc\0rest-of-chars";
    cout << "(1) size=" << fromCharPtrLiteral.size() << ", \"" << fromCharPtrLiteral << "\"\n";

    string fromStringLiteral = "abc\0rest-of-chars"s;
    cout << "(2) size=" << fromStringLiteral.size() << ", \"" << fromStringLiteral << "\"\n";

    return 0;
}


