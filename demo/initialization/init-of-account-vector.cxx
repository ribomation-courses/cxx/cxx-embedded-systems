#include <iostream>
#include <string>
#include <vector>
using namespace std;
using namespace std::literals;

class Account {
    string accno;
    int    balance;
public:
    Account(string a) : accno{a}, balance{100} {}
    Account(int b)    : accno{"XYZ000"}, balance{b} {}
    friend ostream& operator <<(ostream& os, const Account& a) {
        return os << "Account{" << a.accno << ", EUR " << a.balance << "}";
    }
};

int main() {
    vector<Account> accounts{
        Account{"ABC123"}, Account{123}, "DEF456"s, 456
    };
    for (auto const& a : accounts) cout << a << endl;
    return 0;
}


