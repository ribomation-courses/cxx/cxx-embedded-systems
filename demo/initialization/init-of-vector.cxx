#include <iostream>
#include <vector>

using namespace std;
using namespace std::literals;

int main() {
    vector<double> sameType{1.2, 3.4, 5.6};

    vector<double> implicit{1.2, 3,4, 5.6};

    vector<int> syntaxError{1, 2, 3.14, 4, 5};



    vector<int> explicitConversion{1, 2, static_cast<int>(3.14), 4, 5};

    return 0;
}
