#include <iostream>
#include <string>
#include <variant>
#include <vector>
#include <cmath>

using namespace std;
using namespace std::literals;


int main() {
    using Data = variant<long, double, string>;

    Data v = 42L;
    cout << "v<long>: " << get<long>(v) << endl;

    v = 3.1415926;
    cout << "v<double>: " << get<double>(v) << endl;

    v = "hi there"s;
    cout << "v<string>: " << get<string>(v) << endl;

    try {
        get<double>(v);
    } catch (bad_variant_access& x) {
        cout << "(expected) bad_variant_access: " << x.what() << endl;
    }

    if (holds_alternative<long>(v)) {
        cout << "contains a long\n";
    } else if (holds_alternative<string>(v)) {
        cout << "contains a string\n";
    }

    if (auto ptr = get_if<double>(&v); ptr) {
        cout << "*ptr<double>: " << *ptr << endl;
    } else if (auto ptr = get_if<string>(&v); ptr) {
        cout << "*ptr<string>: " << *ptr << endl;
    }

    auto print = [](auto x) { cout << "visit: " << x << endl; };
    visit(print, v);

    v = sqrt(2);
    visit(print, v);

    v = static_cast<double>(get<double>(v) * get<double>(v));
    visit(print, v);

    v = 2L;
    get<long>(v) *= 512;
    visit(print, v);

    cout << "---- values ---\n";
    auto            values = vector<Data>{5L, 2.7, "howdy"s, 42L, 3.14, "hello"s};
    for (auto const& x : values) visit(print, x);


    return 0;
}
