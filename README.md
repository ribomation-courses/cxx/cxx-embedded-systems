C++ for Embedded Systems, 3 days [_On-Site Course_]
====
Welcome to this course in C++ programming.


Here you will find
* Installation instructions
* Solutions to the programming exercises
* Sources to the demo programs

_N.B._ Solutions and Demos programs will be pushed during the course.


Installation Instructions
====

To perform the programming exercises, you need:
* A modern C++ compiler, supporting `C++17`, to compile your code
* An C++ IDE, to write your code
* A GIT client, to get the solutions from this repo

Recommended Setup
----
Although, there are many ways to write and compile C++ programs both 
on Windows, Linux and Mac, our experience of giving many courses on 
various levels; is that using Ubuntu Linux provides the least amount of
surprises and distracting technical struggles.

For this course, we do recommend the following setup:
* Ubuntu Linux, version 19.10
* GCC C++ compiler, version 9
* JetBrains CLion IDE (_30-days trial_)

Ubuntu Linux @ VirtualBox
----

Install VirtualBox, create a virtual machine for Ubuntu and download/install Ubuntu to it.

1. Install VirtualBox `version 6` (VBox)<br/>
    <https://www.virtualbox.org/wiki/Downloads>
1. Create a new virtual machine (VM) i VBox, for Ubuntu Linux<br/>
    <https://www.virtualbox.org/manual/ch03.html>
1. Download an ISO file for the latest version of Ubuntu Desktop 64-bit, `version 19.10`<br/>
    <http://www.ubuntu.com/download/desktop>
1. Choose as much RAM for the VM as you can, still within the "green" region.
1. Choose as much video memory you can
1. Create an auto-expanding virtual hard-drive of (at least) `50GB`
1. Mount the ISO file in the virtual CD drive of your VM
1. Start the VM and run the Ubuntu installation program.
    Ensure you install to the (virtual) hard-drive.
1. Set a username and password when asked to and write them down so you remember them.<br/>
   Also, go for auto-logon.

Inside Ubuntu: install
----
1. Install the VBox `guest additions`<br/>
    <https://www.virtualbox.org/manual/ch04.html><br/>
    Don't forget to reboot, after it's done.
1. Install the GCC compiler and build tools (_see below_)
1. Install GIT and clone this repo (_see below_)
1. Install Clion (_see below_)

GCC C++ Compiler and Tools
----
Within a Ubuntu terminal window type the following command to install the
compiler and other tools.

    sudo apt install g++-9 cmake make gdb valgrind tree

_N.B._ when you run a `sudo` command it prompts you for the password, you use
to logon to Ubuntu.

Change the default GCC version

    sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 90 --slave /usr/bin/g++ g++ /usr/bin/g++-9 --slave /usr/bin/gcov gcov /usr/bin/gcov-9

Verify that GCC version 9, now is the default

    gcc --version
    g++ --version

GIT Client
----
Install GIT and clone this repo

    sudo apt git

Create a base directory for this course and a dedicated sub-folder for your own solutions

    mkdir -p ~/cxx-course/my-solutions
    cd ~/cxx-course
    git clone https://gitlab.com/ribomation-courses/cxx/cxx-embedded-systems.git gitlab

During the course, solutions will be push:ed to this repo and you can get these by a git pull operation

    cd ~/cxx-course/gitlab
    git pull


JetBrains CLion
----

CLion is, in my personal opinion, the best IDE for C/C++ development.
For this course, I do recommend to try it out. You can easily install
it from the application app of Ubuntu(_icon to the left with a big A_).
Type "clion" in the search bar and install it.

After installation, you can find it in the programs menu (_left bottom with 9 dots_). The IDE is really easy to work with, but please check out the 
docs, tutorials and "Quick Tour" video at
* https://www.jetbrains.com/clion/learning-center/


Build Programs
====
The solutions and demo programs are all using CMake as the build tool. 
CMake is a cross platform generator tool that can generate makefiles 
and other build tool files. It is also the project descriptor for 
JetBrains CLion, which is our IDE of choice for C/C++ development.

You don't have to use CLion in order to compile and run the sources. 
What you do need is to have `cmake`, `make` and `gcc/g++` installed
(_see above for installation instructions_). 

Inside a directory with a solution or demo, run the following commands 
to build the program. This will create the executable in the `./build`
directory.

    mkdir build && cd build
    cmake ..
    cmake --build .


Contact Info
====
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.com](https://www.ribomation.com)<br/>

