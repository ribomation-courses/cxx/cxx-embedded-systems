#include <iostream>
#include <iomanip>
#include <cmath>
#include "maclaurin.hxx"

using namespace std;
using namespace ribomation::numeric;

int main() {
    const auto N     = 6U;
    const auto W     = 80U;
    const auto W2    = W / 2;
    const auto lb    = 0.1;
    const auto ub    = 2 * M_PI;
    const auto delta = 0.1;

    for (auto x = lb; x < ub; x += delta) {
        auto y = sinApprox(x, N);
        if (y > 0) {
            auto spaces = W2;
            auto stars  = static_cast<unsigned long>(y * W2);
            cout << fixed << setprecision(3) << x << ": " << string(spaces, ' ') << "|" << string(stars, '*') << endl;
        } else {
            auto spaces = static_cast<unsigned long>(W2 + y * W2) + 1;
            auto stars  = static_cast<unsigned long>(-y * W2);
            cout << fixed << setprecision(3) << x << ": " << string(spaces, ' ') << string(stars, '*') << "|" << endl;
        }
    }

    return 0;
}

