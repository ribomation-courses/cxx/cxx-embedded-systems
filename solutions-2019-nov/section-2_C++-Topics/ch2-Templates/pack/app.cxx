#include <iostream>
#include <iomanip>
#include <bitset>
#include <cstdint>
#include "pack.hxx"

using namespace std;
using namespace ribomation;

int main() {
    auto a = uint8_t{0x12};
    auto A = static_cast<short>(a);
    auto b = uint8_t{0x34};
    auto B = static_cast<short>(b);
    {
        auto result = pack<uint16_t>(a, b);
        cout << hex << "pack<16>(0x" << A << ", 0x" << B << ") = 0x" << setw(10)<<setfill('0') << result
             << " [" << bitset<16>{result} << "]" << endl;
    }
    {
        auto result = pack<uint32_t>(static_cast<uint16_t>(a), static_cast<uint16_t>(b));
        cout << hex << "pack<32>(0x" << A << ", 0x" << B << ") = 0x" << setw(10)<<setfill('0') << result
             << " [" << bitset<32>{result} << "]" << endl;
    }
    {
        auto result = pack<uint64_t>(static_cast<uint32_t>(a), static_cast<uint32_t>(b));
        cout << hex << "pack<64>(0x" << A << ", 0x" << B << ") = 0x" << result
             << " [" << bitset<64>{result} << "]" << endl;
    }
    return 0;
}
