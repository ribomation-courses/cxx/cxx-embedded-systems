#include <iostream>
#include <alloca.h>
using namespace std;
using namespace std::literals;

struct Node {
    unsigned payload;
    Node*    next;
    Node(unsigned x, Node* nxt = nullptr) : payload{x}, next{nxt} {}
};

auto operator<<(ostream& os, const Node& n) -> ostream& {
    return os << n.payload;
}

void print(Node* n) {
    if (n == nullptr) return;
    cout << *n << " ";
    print(n->next);
}

void run(unsigned n) {
    Node* first = nullptr;
    for (auto k = 1U; k <= n; ++k) {
        first = new (alloca(sizeof(Node))) Node{k, first};
    }
    print(first);
    cout << endl;
}

int main() {
    run(5);
    run(10);
    run(20);
    return 0;
}
