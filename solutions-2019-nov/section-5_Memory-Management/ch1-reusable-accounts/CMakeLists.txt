cmake_minimum_required(VERSION 3.10)
project(ch16_using_new_and_delete)

set(CMAKE_CXX_STANDARD 17)
add_compile_options(-Wall -Wextra -Werror -Wfatal-errors)

add_executable(accounts
        account.hxx account.cxx
        app.cxx)
