#include <iostream>
#include "account.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation::types;

void useThreeObjects() {
    cout << "--- Use 3 Objects ---\n";
    auto acc1 = new Account{100, 0.5};
    cout << "acc1: " << *acc1 << endl;

    auto acc2 = new Account{200, 1.5};
    cout << "acc2: " << *acc2 << endl;

    auto acc3 = new Account{300, 2.5};
    cout << "acc3: " << *acc3 << endl;

    delete acc3;
    delete acc2;
    delete acc1;
}

void useAllObjects() {
    cout << "--- Use ALL Objects ---\n";
    vector<Account*> accounts;
    const int        N = static_cast<int>(Account::capacity());

    for (auto k = 0; k < N; ++k) {
        accounts.push_back(new Account{k * 100 + 50, static_cast<float>(k * 0.2 + 0.1)});
    }

    for (auto a : accounts) cout << *a << endl;

    while (!accounts.empty()) {
        auto a = accounts.back();
        accounts.pop_back();
        delete a;
    }
}

void triggerOverflow() {
    cout << "--- Trigger Overflow ---\n";
    vector<Account*> accounts;
    const int        N = 1 + static_cast<int>(Account::capacity());

    try {
        for (auto k = 0; k < N; ++k) {
            accounts.push_back(new Account{10, 0.1F});
        }
    } catch (overflow_error& x) {
        cout << "\n** error: " << x.what() << endl;
    }
}

int main() {
    useThreeObjects();
    useAllObjects();
    triggerOverflow();
    return 0;
}
