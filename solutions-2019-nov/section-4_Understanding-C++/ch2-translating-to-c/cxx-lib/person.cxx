#include <iostream>
#include <algorithm>
#include <string>
#include <cstring>
#include <cctype>
#include "person.hxx"

Person* createPerson(const char* s, unsigned age) {
    std::string name{s};
    return new Person{name, age};
}

Person::Person(std::string name, unsigned age) : name{name}, age{age} {
    std::cout << "Person(" << name << ", " << age << ") @ " << this << std::endl;
}

Person::~Person() {
    std::cout << "~Person() @ " << this << std::endl;
}

const char* Person::getName() const {
    return name.c_str();
}

unsigned Person::getAge() const {
    return age;
}

unsigned Person::incrAge() {
    return ++age;
}
