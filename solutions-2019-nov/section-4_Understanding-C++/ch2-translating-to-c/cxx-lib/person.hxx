#pragma once

#include <string>

class Person {
    const std::string name;
    unsigned     age;
public:
    Person(std::string name, unsigned age);
    ~Person();
    const char* getName() const;
    unsigned getAge() const;
    unsigned incrAge();
};

Person* createPerson(const char* s, unsigned age);

