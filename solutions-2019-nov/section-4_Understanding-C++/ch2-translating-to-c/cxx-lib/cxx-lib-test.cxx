#include <iostream>
#include "string-util.hxx"
#include "person.hxx"
using namespace std;

int main() {
    auto s = toUpperCase("Foobar strikes again");
    cout << "s: " << s << endl;
    delete s;

    Person* p = createPerson("Anna Conda", 42);
    cout << "p: " << p->getName() << ", " << p->getAge() << endl;
    delete p;
    
    return 0;
}

