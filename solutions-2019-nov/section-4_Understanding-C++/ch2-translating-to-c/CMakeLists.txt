cmake_minimum_required(VERSION 3.10)
project(ch11_translating_to_c)

set(CMAKE_CXX_STANDARD  17)
set(CMAKE_C_STANDARD    99)
add_compile_options(-Wall -Wextra -Werror -Wfatal-errors)

add_executable(cxx-lib-test
        ./cxx-lib/string-util.hxx
        ./cxx-lib/string-util.cxx
        cxx-lib/person.hxx
        cxx-lib/person.cxx
        ./cxx-lib/cxx-lib-test.cxx)

add_executable(using-touppercase
        ./cxx-lib/string-util.hxx
        ./cxx-lib/string-util.cxx
        c-lib/using-touppercase.c
        )

add_executable(using-person
        cxx-lib/person.hxx
        cxx-lib/person.cxx
        c-lib/using-person.c)


