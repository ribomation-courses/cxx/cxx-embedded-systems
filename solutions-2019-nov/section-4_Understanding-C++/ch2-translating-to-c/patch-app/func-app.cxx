#include <iostream>
#include "func.hxx"
using namespace std;

int main() {
    auto n   = 42U;
    auto res = magic(n);
    cout << "magic(" << n << ") = " << res << endl;
    return 0;
}
