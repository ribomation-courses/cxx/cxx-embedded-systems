#include <stdio.h>
#include <stdlib.h>

struct Person;
extern struct Person*   _Z12createPersonPKcj(const char*, unsigned);
extern const char*      _ZNK6Person7getNameEv(struct Person*);
extern unsigned         _ZNK6Person6getAgeEv(struct Person*);
extern unsigned         _ZN6Person7incrAgeEv(struct Person*);
extern void*            _ZN6PersonD1Ev(struct Person*);

int main() {
    struct Person* p = _Z12createPersonPKcj("Anna Conda", 42);
    
    printf("[C] Person{%s, %d}\n", _ZNK6Person7getNameEv(p), _ZNK6Person6getAgeEv(p));
    _ZN6Person7incrAgeEv(p);
    printf("[C] Person{%s, %d}\n", _ZNK6Person7getNameEv(p), _ZNK6Person6getAgeEv(p));
    
    free(_ZN6PersonD1Ev(p));

    return 0;
}

