#include <stdio.h>
#include <stdlib.h>

extern const char* _Z11toUpperCasePKc(const char*);

int main() {
    const char* s = _Z11toUpperCasePKc("Tjolla Hopp");
    printf("[C] s: %s\n", s);
    free((void*) s);
    return 0;
}
