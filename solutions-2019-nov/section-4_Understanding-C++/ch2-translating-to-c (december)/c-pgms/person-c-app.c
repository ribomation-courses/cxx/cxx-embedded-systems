#include <stdio.h>
#include <stdlib.h>

struct Person;
extern struct Person* _Z9newPersonPKcj(const char*, unsigned);
extern void* _ZN6PersonD1Ev(struct Person*);
const char* _ZNK6Person7getNameEv(struct Person*);
extern unsigned _ZNK6Person6getAgeEv(struct Person*);
extern void _ZN6Person7incrAgeEv(struct Person*);

int main(int argc, char** argv) {
    struct Person* p = _Z9newPersonPKcj("Justin Time", 42);
    printf("p: %s, %d @ %p\n", _ZNK6Person7getNameEv(p), _ZNK6Person6getAgeEv(p), p);
    _ZN6Person7incrAgeEv(p);
    printf("p: %s, %d @ %p\n", _ZNK6Person7getNameEv(p), _ZNK6Person6getAgeEv(p), p);
    free(_ZN6PersonD1Ev(p));

    return 0;
}
