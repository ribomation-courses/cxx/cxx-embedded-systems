extern void unit1(int);
extern void unit2(int);

//multiple definition of `magic(int)'
//error: ld returned 1 exit status

int main() {
    unit1(10);
    unit2(10);
    return 0;
}
