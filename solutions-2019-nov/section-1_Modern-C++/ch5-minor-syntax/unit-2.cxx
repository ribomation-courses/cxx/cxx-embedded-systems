#include <iostream>

namespace {
    void magic(int n) {
        std::cout << "magic-2: " << n * 10 << "\n";
    }
}

void unit2(int n) {
    magic(n);
}
