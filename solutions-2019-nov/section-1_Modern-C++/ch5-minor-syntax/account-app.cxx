#include <iostream>
#include <string>

using namespace std;
using namespace std::literals;

enum class AccountType {
    TRANSFER, SAVINGS, CURRENCY
};

auto toString(AccountType type) {
    switch (type) {
        case AccountType::TRANSFER:
            return "TRANSFER"s;
        case AccountType::SAVINGS:
            return "SAVINGS"s;
        case AccountType::CURRENCY:
            return "CURRENCY"s;
    }
    return "???"s;
}

class Account {
    AccountType type    = AccountType::TRANSFER;
    int         balance = 100;
public:
    Account() = default;
    Account(const Account&) = delete;
    auto operator =(const Account&) -> Account& = delete;

    auto operator +=(int amount) -> Account& {
        balance += amount;
        return *this;
    }

    friend auto operator <<(ostream& os, const Account& acc) -> ostream& {
        return os << "Account{" << toString(acc.type) << ", EUR " << acc.balance << "}";
    }
};

void func(Account obj) {
    cout << obj << endl;
}

void func2(Account& obj) {
    obj += 42;
    cout << obj << endl;
}

int main() {
    auto acc = Account{};
    cout << acc << endl;
    //func(acc); //error: use of deleted function "Account::Account(const Account&)"
    func2(acc);
    return 0;
}
