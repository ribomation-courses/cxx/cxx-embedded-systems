#include <iostream>

namespace {
    void magic(int n) {
        std::cout << "magic-1: " << n * 42 << "\n";
    }
}

void unit1(int n) {
    magic(n);
}
