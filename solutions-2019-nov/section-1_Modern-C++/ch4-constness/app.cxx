#include <iostream>
using namespace std;

class Account {
    int balance = 42;
public:
    int get() const {return balance;}
    void update(int amount) {balance += amount;}
};

void print(const Account& acc) {
    cout << "Account{EUR " << acc.get() << "}" << endl;
}

void modify(const Account& acc) {
    auto amount = -15;
    //acc.update(amount); //error: passing "const Account" as "this" argument discards qualifiers
    const_cast<Account&>(acc).update(amount);
}

int main() {
    auto account = Account{};
    print(account);
    modify(account);
    print(account);
    return 0;
}
