#include "person.hxx"

namespace {
    char* copystr(const char* str) {
        if (str == nullptr) return nullptr;
        return strcpy(new char[strlen(str) + 1], str);
    }

    auto asString(const char* s) -> const char* {
        return s == nullptr ? "''" : s;
    }
}

namespace ribomation {
    int Person::instanceCount = 0;

    void Person::dump(const char* type, ostream& os) {
        os << (type[0] == '~' ? "~" : "")
           << "Person(" << (type[0] == '~' ? "" : type) << ") "
           << "name=" << asString(name) << ", age=" << age
           << " @ " << this
           << " [" << instanceCount << "]"
           << "\n";
    }

    Person::Person()
            : name{copystr("")}, age{0} {
        ++instanceCount;
        dump("", cout);
    }

    Person::Person(const char* n, unsigned a)
            : name{copystr(n)}, age{a} {
        ++instanceCount;
        dump("char*,int", cout);
    }

    Person::Person(const Person& that)
            : name{copystr(that.name)}, age{that.age} {
        ++instanceCount;
        dump("const Person&", cout);
    }

    Person::Person(Person&& that) noexcept
            : name{that.name}, age{that.age} {
        ++instanceCount;
        that.name = nullptr;
        that.age  = 0;
        dump("Person&&", cout);
    }

    Person::~Person() {
        --instanceCount;
        dump("~", cout);
        delete[] name;
    }

    auto Person::operator =(const Person& that) -> Person& {
        if (this != &that) {
            delete[] name;
            name = copystr(that.name);
            age  = that.age;
        }
        cout << "=(const Person&) name=" << name << ", age=" << age << " @ " << this << endl;
        return *this;
    }

    auto Person::operator =(Person&& that) noexcept -> Person& {
        if (this != &that) {
            delete[] name;
            name = that.name;
            that.name = nullptr;
            age = that.age;
            that.age = 0;
        }
        cout << "=(Person&&) name=" << name << ", age=" << age << " @ " << this << endl;
        return *this;
    }

    unsigned Person::incrAge() {
        return ++age;
    }

    string Person::toString() const {
        ostringstream buf{};
        buf << "Person(" << asString(name) << ", " << age << ") @ " << this << "";
        return buf.str();
    }

}
